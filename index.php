<?php require_once 'geoip.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trade with the Pros at PhoenixMarkets</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />

    <?php echo "<link rel='stylesheet' type='text/css' href='assets/css/style.css?'".mt_rand().">";?>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

 <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M7LW664');</script>
<!-- End Google Tag Manager -->

</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7LW664" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Server Api Response Error -->
    <?php if (isset($_GET['error'])) {
    $error = $_GET['error'];
  ?> <script type="text/javascript">
    $(document).ready(function() {
        $("#server_error_msg").show();
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 4000);
    })
    </script>
    <?php } else {
    $error = ""; ?>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#server_error_msg").hide();
    })
    </script>
    <?php } ?>


    <!-- Hero Section -->
    <section class="hero_section bg-dark text-light">
        <div class="container">
            <div class="row d-flex ">
                <div class="col-lg-6 col-12 col-left aligns-items-center text-center border-warning">
                    <div class="border-warning-mob">
                        <div class="logo pt-3">
                            <a target="_blank" href="#"><img src="assets/img/logo.png" alt="Logo"></a>
                        </div>

                        <div class="site_title pt-5 pb-5">
                            <a target="_blank" href="#"><img src="assets/img/Investing_1.png" alt="Investing.Com"></a>
                            <p style="font-size: 28px; margin-top: -30px; font-family: Proxima Nova;margin-bottom: 0px;">Trusted Partner</p>
                        </div>


                        <!-- <p>Vous avez déjà un compte? <a href="#">Connexion</a></p> -->
                        <!-- form section -->
                        <div class="signUp_form">
                            <form class="form" action="form.php" method="POST" onsubmit="return Validation()">

                                <!-- Api Response Error -->
                                <div class="alert alert-danger alert-dismissible" role="alert"
                                    id="server_error_msg">
                                    <strong>Error!</strong> <?php echo $error; ?>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                </div>

                                <!-- Click id -->
                                <?php if (isset($_GET['clickid']) && ($_GET['clickid'] != "")) { ?>
                                <input type="hidden" value="<?php echo $_GET['clickid']; ?>" name="clickid" />
                                <?php  } ?>
                                <!-- getting full countryName -->
                                <input type="hidden" id="countryName" name="countryName"
                                    value="<?= @$geo['countryName']?>">

                                <span id="fname-message" class="errors-message"></span>
                                <div class="input-group with-icon icon-shy pb-3">
                                    <input class="form-control p-3" name="FirstNameLastName" type="text" id="fname"
                                        placeholder="Enter your full name" required onkeyup="checkName();">
                                    <i class="fa fa-user"></i>
                                </div>

                                <span id="email-message" class="errors-message"></span>
                                <div class="input-group with-icon icon-shy pb-3">
                                    <input class="form-control p-3" name="email" type="email" id="email"
                                        placeholder="E-mail" required onkeyup="">
                                    <i class="fa fa-envelope"></i>
                                </div>

                                <span id="password-message" class="errors-message"></span>
                            <div class="input-group with-icon icon-shy pb-3">
                                <input class="form-control p-3" name="password" type="password" id="password" placeholder="password" required onkeyup="check_password();">
                                <i class="fa fa-lock"></i>
                            </div>

                                <span id="phone-message" class="errors-message"></span>
                                <div class="input-group with-icon icon-shy">
                                    <input class="form-control tel  p-3 phone" name="phone" id="phone" type="tel"
                                        placeholder="" required>
                                    <i class="fa fa-phone pt-3"></i>
                                </div>
                                <!-- <span id="checkbox-error" class="errors-message">Accept terms and conditon is required</span> -->
                                <br />
                                <div class="form-group text-md-start">
                                    <div class="row">
                                        <div class="col-1">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="checked" required
                                                    checked>
                                            </div>
                                        </div>
                                        <div class="col-11">
                                            <label class="form-check-label" for="flexCheckDefault">
                                            I am over 18 and have read and agreed with the Phoenix Markets T&Cs as well as the Privacy Policy.
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group pt-3 pb-3 btn-register">
                                    <button type="submit" class="btn btn-warning btn-lg" id="commence-btn">Start Trading Now <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                                            fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd"
                                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                        </svg></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <!-- Slider/Carousel -->
                <div class="col-lg-6 col-12 ps-0 col-right">

                    <div id="carouselExampleCaptions" class="carousel carousel-h100 slide" data-bs-ride="carousel">
                        <div class="carousel-indicators pb-4">
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active" data-bs-interval="3000">
                                <img src="assets/img/slide1.png" class="d-block w-100"
                                    alt="PHOENIX. VOTRE COURTIER POUR TRADER">
                                <div class="carousel-caption">
                                    <h5>Discover Why Traders Are Turning To Algorithmic Trading</h5>
                                    <p>We know how hard getting the right trades can be, especially if you’re new to trading.
                                         To help out all our new traders, we’ve given them access to the Phoenix Algorithmic 
                                         Trading System, worth over €500 yearly. With our algorithmic trading system, you can 
                                         select from a series of built-in trading strategies or build your own with ease.</p>
                                         <br>
                                </div>
                            </div>
                            <div class="carousel-item" data-bs-interval="3000">
                                <img src="assets/img/slide2.png" class="d-block w-100"
                                    alt="AUTOMATISEZ VOS STRATÉGIES. TRADING ALGORITHMIQUE ICI!">
                                <div class="carousel-caption">
                                    <h5>AUTOMATE YOUR STRATEGIES. ALGORITHMIC TRADING HERE!</h5>
                                    <p>You are here because you want to trade. So go ahead, fill in first
                                        the question sheet. Trade Forex, Indices, Stocks, Commodities and
                                        Cryptos as you like, hassle-free.</p>
                                        <br>
                                </div>
                            </div>
                            <div class="carousel-item" data-bs-interval="3000">
                                <img src="assets/img/slide3.png" class="d-block w-100"
                                    alt="GAGNEZ DES INTERÊTS IMMÉDIATEMENT APRÉS VOTRE PREMIER DÉPÔT!">
                                <div class="carousel-caption">
                                    <h5>EARN INTEREST IMMEDIATELY AFTER YOUR FIRST DEPOSIT!</h5>
                                    <p>As soon as you make your first deposit by bank transfer, we'll add
                                        tradable and withdrawable interest on your trading account!</p>
                                        <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row End -->

            <!-- Footer Row -->

            <section class="footer">
                <div class="col">
                    <div class="row">
                        <div class="col-md-4 col-12 pb-3">
                            <a target="_blank" href="./docs/regulation/politiques/KID_Indices_fr.pdf"> KEY INFORMATION DOCUMENT</a>
                        </div>

                        <div class="col-md-4 col-12 pb-3">
                            <a target="_blank" href="./docs/regulation/politiques/Divulgation_des_risques.pdf">RISK DISCLOSURE</a>
                        </div>

                        <div class="col-md-4 col-12 pb-3">
                            <a target="_blank" href="./docs/regulation/politiques/Termes_et_conditions.pdf"> TERMS</a>
                        </div>
                    </div>
                    <p class="para">
                    HIGH RISK INVESTMENT WARNING: Investments are complex instruments and come with a high risk of losing money
                     rapidly due to leverage. 82% of retail investor accounts lose money when trading Investments with this provider.
                      You should consider whether you understand how Investments work and whether you can afford to take the high
                       risk of losing your money. Click here to read our Risks Disclosure. Phoenix Markets is the trading name
                        of WGM Services Ltd, a financial services company subject to the regulation and supervision of the Cyprus 
                        Securities Exchange Commission (CySEC) under license number 203/13. The offices of WGM Services Ltd are 
                        located at 11, Vizantiou, 4th Floor, Strovolos 2064, Nicosia, Cyprus.
                        The information and services contained on this website are not intended for 
                        residents of the United States or Canada.
                    </p>
                </div>

            </section>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <script>
    const phoneInputField = document.querySelector("#phone");
    const phoneInput = window.intlTelInput(phoneInputField, {
        initialCountry: "auto",
        geoIpLookup: function(success) {
            // Get your api-key at https://ipdata.co/
            fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
                .then(function(response) {
                    if (!response.ok) return success("");
                    return response.json();
                })
                .then(function(ipdata) {
                    success(ipdata.country_code);
                });
        },
        utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
    });

    $('#commence-btn').click(function() {
        var code = phoneInput.getNumber();
        $('#phone').val(code);
    });

    // phone number validation
    $('#phone').first().keyup(function() {
        var phone = this.value;
        if (phone.length < 5) {
            document.getElementById("phone-message").innerHTML = "This field is required.";
        } else {
            document.getElementById("phone-message").innerHTML = " ";
            return true;
        }
    });


    //   validation function
    function Validation() {
        var pw = document.getElementById("password").value;
        if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
            document.getElementById("password-message").innerHTML =
                "Password length must be at least 8 characters. Password must contain at least one letter, one uppercase and one number";
            return false;
        }
        if (pw.length > 15) {
            document.getElementById("password-message").innerHTML =
                "Password length must not exceed 15 characters.";
            return false;
        }

        var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
        var fname = document.getElementById("fname").value;
        if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname)) {
            document.getElementById("fname-message").innerHTML = "The name must be followed by First name, Space then Last name.";
            return false;
        }

        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var emails = document.getElementById("email").value;
        if ((emails.length < 2) || (!emailReg.test(emails))) {
            document.getElementById("email-message").innerHTML =
                "This field is required, please enter a valid email address";
            return false;
        }

        var phone = document.getElementById("phone").value;
        if (phone.length < 5) {
            document.getElementById("phone-message").innerHTML = "This field is required.";
            return false;
        }
    }
    </script>

    <script>
    var checkName = function() {
        var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
        var fname = document.getElementById("fname").value;
        if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ")
                .length > 2)) {
            document.getElementById("fname-message").innerHTML =
                "The name must be followed by First name, Space then Last name.";
            return false;
        }
        document.getElementById("fname-message").innerHTML = " ";
        return true;
    }
    </script>


    <script>
    $('#email').first().keyup(function() {
        var $email = this.value;
        validateEmail($email);
    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if ((email.length < 2) || (!emailReg.test(email))) {
            document.getElementById("email-message").innerHTML = "This field is required.Please enter a valid email";
        } else {
            document.getElementById("email-message").innerHTML = " ";
            return true;
        }
    }
    </script>

    <script>
    var check_password = function() {
        var pw = document.getElementById("password").value;
        if (pw.length < 8) {
            document.getElementById("password-message").innerHTML =
                "Password length must be at least 8 characters.";
            return false;
        }
        if ((pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
            document.getElementById("password-message").innerHTML =
                "Password must contain at least one letter, one uppercase and one number";
            return false;
        }
        document.getElementById("password-message").innerHTML = " ";
        return true;
    }
    </script>

</body>

</html>